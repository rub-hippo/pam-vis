.. helper

Utilities & helper
==================

.. currentmodule:: pam.helper

.. autofunction:: pam.helper.random_select_indices
.. autofunction:: pam.helper.accumulate
.. autofunction:: pam.helper.transformed_objects
.. autofunction:: pam.helper.uv_pixel_values
